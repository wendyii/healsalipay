import Vue from 'vue'
import Router from 'vue-router'
import transformindex from '@/components/lang/transformindex.vue'
import splash from '@/components/users/splash.vue'
import login from '@/components/users/login.vue'
import register from '@/components/users/register.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/splash'
    },
    {
      path: '/transformindex',
      name: 'transformindex',
      component: transformindex
    },
    {
      path: '/splash',
      name: 'splash',
      component: splash
    },
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/register',
      name: 'register',
      component: register
    }
  ]
})
