// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueI18n from 'vue-i18n'
import App from './App'
import router from './router'

Vue.config.productionTip = false
Vue.use(VueI18n)

const i18n = new VueI18n({
  locale: 'ZHS',
  messages: {
    'EN': require('@/assets/i18n/en.js'),
    'ZHS': require('@/assets/i18n/zh-HANS.js'),
    'ZHT': require('@/assets/i18n/zh-HANT.js')
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  i18n,
  router,
  components: { App },
  template: '<App/>'
})
